       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. ANANCHAI MUENSAI.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  HEIGHT   PIC 999.
       01  WEIGHT   PIC 999.
       01  BMI      PIC 99V99.
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter Height (CM)"
           ACCEPT HEIGHT 
           DISPLAY "Enter Weight (KG)"
           ACCEPT WEIGHT 
           COMPUTE BMI = WEIGHT / ((HEIGHT/100) * 2)

           DISPLAY "Your BMI is :" BMI

           IF BMI < 18.5 THEN
              DISPLAY "Underweight"
           END-IF 

           IF BMI >= 18.5 AND < 24.9 THEN
              DISPLAY "Normal weight"
           END-IF

           IF BMI >= 25 AND < 29.9 THEN
              DISPLAY "Overweight"
           END-IF

           IF BMI > 30 THEN
              DISPLAY "Obesity"
           END-IF
           .
